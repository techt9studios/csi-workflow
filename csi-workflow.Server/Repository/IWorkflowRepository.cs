using System.Collections.Generic;
using System.Threading.Tasks;
using csi_workflow.Shared;

namespace csi_workflow.Server.Repository
{
    public interface IWorkflowRepository
    {
         Task<List<Workflows>> GetWorkflowsAsync(int id, string status);
         Task<List<Workflows>> GetWorkflowAsync(int id);
         Task<List<Workflows>> PutWorkflowAsync(Workflows workflow);
    }
}