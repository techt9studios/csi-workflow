using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using csi_workflow.Shared;

namespace csi_workflow.Server.Repository
{
    public class GroupRepository : IGroupRepository
    {
        private readonly ILogger _logger;
        private readonly CSIDbContext _context;

        public GroupRepository(CSIDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger(nameof(GroupRepository));
        }


        public async Task<List<Groups>> GetGroupsAsync(int groupOnly, int groupId)
        {
            string sql = $"EXEC dbo.CSIWGetGroups {groupOnly}, {groupId} " ;
            return await _context.Groups.FromSqlRaw(sql).ToListAsync();        
        }        
    }
}