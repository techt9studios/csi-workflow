using System;
using csi_workflow.Shared;
using Microsoft.EntityFrameworkCore;


namespace csi_workflow.Server.Repository
{
    public class CSIDbContext : DbContext
    {
        public DbSet<Groups> Groups { get; set; }
        public DbSet<Workflows> Workflows { get; set; }
        public CSIDbContext(DbContextOptions<CSIDbContext> options) : base(options) { }
    }
}