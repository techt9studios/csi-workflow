using System.Collections.Generic;
using System.Threading.Tasks;
using csi_workflow.Shared;

namespace csi_workflow.Server.Repository
{
    public interface IGroupRepository
    {
         Task<List<Groups>> GetGroupsAsync(int groupOnly, int groupId);
    }
}