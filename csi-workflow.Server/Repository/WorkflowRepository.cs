using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using csi_workflow.Shared;
using System.Data.SqlClient;

namespace csi_workflow.Server.Repository
{
    public class WorkflowRepository : IWorkflowRepository
    {
        private readonly ILogger _logger;
        private readonly CSIDbContext _context;

        public WorkflowRepository(CSIDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger(nameof(WorkflowRepository));
        }


        public async Task<List<Workflows>> GetWorkflowsAsync(int id, string status)
        {
            string sql = $"EXEC dbo.CSIWGetWorkflow {id}, '{status}' "; 
            return await _context.Workflows.FromSqlRaw(sql).ToListAsync();        
        }   

        public async Task<List<Workflows>> GetWorkflowAsync(int id)
        {
            string sql = $"EXEC dbo.CSIWGetSingleWorkflow {id}"; 
            return await _context.Workflows.FromSqlRaw(sql).ToListAsync();        
        }


        public async Task<List<Workflows>> PutWorkflowAsync(Workflows workflow)
        {
            string sql = $"EXEC dbo.CSIWUpdateSingleWorkflow {workflow.Id}, '{workflow.DueDate}', {workflow.Priority}, "
                + $"'{workflow.Owner}', {workflow.Hours}, {workflow.Completed}, '{workflow.CompletedDate}', '{workflow.Notes}', {workflow.OwnerId}";

            return await _context.Workflows.FromSqlRaw(sql).ToListAsync();
        }          
    }
}