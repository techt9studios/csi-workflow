USE [PG_App]
GO
/****** Object:  StoredProcedure [dbo].[CSIWUpdateSingleWorkflow]    Script Date: 6/7/2019 1:59:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[CSIWUpdateSingleWorkflow]

	@id INT,
	@DueDate SMALLDATETIME = NULL,
	@Priority INT = NULL,
	@Owner NVARCHAR(50) = NULL,
	@Hours DECIMAL(9,2) = NULL,
	@Completed BIT = 0,
	@CompletedDate SMALLDATETIME = NULL,
	@Notes NVARCHAR(MAX) = NULL,
	@OwnerId INT = NULL

AS

	SET @Owner = (SELECT a.Username FROM UserNames a WHERE UserId = @OwnerId)

	UPDATE csi
		SET csi.DueDate = @DueDate,
		csi.Priority = @Priority,
		csi.Owner = @Owner,
		csi.Hours = @Hours,
		csi.Completed = @Completed,
		csi.CompletedDate = @CompletedDate,
		csi.Notes = @Notes,
		csi.OwnerId = @OwnerId
	FROM CSIWTransactions csi
	WHERE csi.Id = @id


	IF @Completed = 1
	BEGIN
		UPDATE em
			SET em.Userid = @OwnerId 
		FROM EventMessage em
		INNER JOIN CSIWTransactions cs
		ON em.RowPointer = cs.CSIId
		WHERE cs.Id = @id
	END

	SELECT Id, 
		Task, 
		SalesOrder, 
		SalesOrderLine, 
		ItemNumber, 
		ItemDescription, 
		DueDate, 
		Priority, 
		CreatedBy,
		Owner, 
		Hours, 
		Completed, 
		CompletedDate, 
		UserId, 
		CSIId, 
		Notes,
		OwnerId 
	FROM CSIWTransactions 
	WHERE Id = @id









GO
