USE [PG_App]
GO
/****** Object:  Table [dbo].[CSIWTransactions]    Script Date: 6/7/2019 1:59:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CSIWTransactions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Task] [nvarchar](255) NULL,
	[SalesOrder] [nvarchar](20) NULL,
	[SalesOrderLine] [int] NULL,
	[ItemNumber] [nvarchar](30) NULL,
	[ItemDescription] [nvarchar](50) NULL,
	[DueDate] [smalldatetime] NULL,
	[Priority] [int] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Owner] [nvarchar](50) NULL,
	[Hours] [decimal](9, 2) NULL,
	[Completed] [bit] NULL,
	[CompletedDate] [smalldatetime] NULL,
	[UserId] [int] NULL,
	[CSIId] [nvarchar](100) NULL,
	[Notes] [nvarchar](max) NULL,
	[OwnerId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
