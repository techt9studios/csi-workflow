USE [PG_App]
GO
/****** Object:  StoredProcedure [dbo].[CSIWGetSingleWorkflow]    Script Date: 6/7/2019 1:59:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[CSIWGetSingleWorkflow]

	@id int

AS

	SELECT Id, 
		Task, 
		SalesOrder, 
		SalesOrderLine, 
		ItemNumber, 
		ItemDescription, 
		ISNULL(DueDate, '1900-01-01 00:00:00') DueDate, 
		Priority, 
		CreatedBy,
		Owner, 
		Hours, 
		Completed, 
		ISNULL(CompletedDate, '1900-01-01 00:00:00') CompletedDate, 
		UserId, 
		CSIId, 
		Notes,
		OwnerId 
	FROM CSIWTransactions 
	WHERE Id = @id







GO
