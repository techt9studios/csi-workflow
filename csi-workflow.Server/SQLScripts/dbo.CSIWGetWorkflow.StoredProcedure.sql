USE [PG_App]
GO
/****** Object:  StoredProcedure [dbo].[CSIWGetWorkflow]    Script Date: 6/7/2019 1:59:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[CSIWGetWorkflow]

	@id INT,
	@status NVARCHAR(20)

AS
	INSERT INTO CSIWTransactions 
	(
		Task, 
		SalesOrder, 
		SalesOrderLine, 
		ItemNumber, 
		ItemDescription, 
		DueDate, 
		Priority, 
		CreatedBy,
		Owner, 
		Hours, 
		Completed, 
		CompletedDate, 
		UserId, 
		CSIId, 
		Notes,
		OwnerId
	)
	SELECT 
		--CAST(ROW_NUMBER() OVER(ORDER BY em.RowPointer ASC) AS INT)  Id,
		ehr.Description Task,
		(SELECT Value 
			FROM EventParameter ep 
			WHERE es.EventParmId = ep.EventParmId 
				AND SUBSTRING(Name, CHARINDEX('.', Name, 1) + 1, LEN(Name) - CHARINDEX('.', Name, 1)) = 'CoNum') SalesOrder,
		CAST((SELECT Value 
			FROM EventParameter ep 
			WHERE es.EventParmId = ep.EventParmId 
				AND SUBSTRING(Name, CHARINDEX('.', Name, 1) + 1, LEN(Name) - CHARINDEX('.', Name, 1)) = 'CoLine') AS INT) SalesOrderLine,
		(SELECT Value 
			FROM EventParameter ep 
			WHERE es.EventParmId = ep.EventParmId 
				AND SUBSTRING(Name, CHARINDEX('.', Name, 1) + 1, LEN(Name) - CHARINDEX('.', Name, 1)) = 'Item') ItemNumber,
		(SELECT Value 
			FROM EventParameter ep 
			WHERE es.EventParmId = ep.EventParmId 
				AND SUBSTRING(Name, CHARINDEX('.', Name, 1) + 1, LEN(Name) - CHARINDEX('.', Name, 1)) = 'Description') ItemDescription,
		'1900-01-01 00:00:00' DueDate,
		0 [Priority],
		em.CreatedBy,
		'' Owner,
		CAST(0 AS DECIMAL(9,2)) Hours,
		CAST(0 AS BIT) Completed,
		'1900-01-01 00:00:00' CompletedDate,
		CAST(em.Userid AS INT) UserId,
		CAST(em.RowPointer AS NVARCHAR(100)) CSIId,
		'' Notes,
		CAST(em.Userid AS INT) OwnerId
	FROM EventMessage em
	INNER JOIN EventActionState eas
		ON em.EventActionStateRowPointer = eas.RowPointer
	INNER JOIN EventHandlerState ehs
		ON eas.EventHandlerStateRowPointer = ehs.RowPointer
	INNER JOIN EventState es
		ON ehs.EventStateRowPointer = es.RowPointer
	INNER JOIN EventAction ea
		ON eas.EventActionRowPointer = ea.RowPointer
	INNER JOIN EventHandlerRevision ehr
		ON ea.EventHandlerRowPointer = ehr.RowPointer
	WHERE em.userid = @id
		AND NOT EXISTS (SELECT csiw.CSIId 
							FROM CSIWTransactions csiw 
							WHERE em.RowPointer = csiw.CSIId)

IF @status = 'Open'
BEGIN
	SELECT Id, 
		Task, 
		SalesOrder, 
		SalesOrderLine, 
		ItemNumber, 
		ItemDescription, 
		ISNULL(DueDate, '1900-01-01 00:00:00') DueDate,
		Priority, 
		CreatedBy,
		Owner, 
		Hours, 
		Completed, 
		ISNULL(CompletedDate, '1900-01-01 00:00:00') CompletedDate, 
		UserId, 
		CSIId, 
		Notes,
		OwnerId
	FROM CSIWTransactions 
	WHERE UserId = @id
		AND Completed = 0
END

IF @status = 'Completed'
BEGIN
	SELECT Id, 
		Task, 
		SalesOrder, 
		SalesOrderLine, 
		ItemNumber, 
		ItemDescription, 
		ISNULL(DueDate, '1900-01-01 00:00:00') DueDate,
		Priority, 
		CreatedBy,
		Owner, 
		Hours, 
		Completed, 
		ISNULL(CompletedDate, '1900-01-01 00:00:00') CompletedDate, 
		UserId, 
		CSIId, 
		Notes,
		OwnerId
	FROM CSIWTransactions 
	WHERE UserId = @id
		AND Completed = 1
END

IF @status = 'All'
BEGIN
	SELECT Id, 
		Task, 
		SalesOrder, 
		SalesOrderLine, 
		ItemNumber, 
		ItemDescription, 
		ISNULL(DueDate, '1900-01-01 00:00:00') DueDate,
		Priority, 
		CreatedBy,
		Owner, 
		Hours, 
		Completed, 
		ISNULL(CompletedDate, '1900-01-01 00:00:00') CompletedDate, 
		UserId, 
		CSIId, 
		Notes,
		OwnerId
	FROM CSIWTransactions 
	WHERE UserId = @id
		AND Completed IN (0, 1)
END








GO
