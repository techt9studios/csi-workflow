USE [PG_App]
GO
/****** Object:  StoredProcedure [dbo].[CSIWGetGroups]    Script Date: 6/7/2019 1:59:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[CSIWGetGroups]

	@groupOnly INT = 0,
	@groupId INT = NULL

AS

IF @groupOnly = 0
BEGIN
	SELECT 
		CAST(ROW_NUMBER() OVER(ORDER BY gn.GroupId ASC) AS INT)  Id,
		CAST(gn.GroupId AS INT) GroupId,
		gn.GroupName, 
		gn.GroupDesc,
		CAST(un.UserId AS INT) UserId,
		un.Username,
		un.UserDesc
	FROM GroupNames gn 
	INNER JOIN UserGroupMap_mst ugm
	ON gn.GroupId = ugm.GroupId
	INNER JOIN Usernames un
	ON ugm.UserId = un.UserId
	WHERE SUBSTRING(gn.GroupName, 1, 4) = 'CSIW'
	AND (@groupId = 0 OR gn.GroupId = @groupId)
END

IF @groupOnly = 1
BEGIN
	SELECT 
		CAST(ROW_NUMBER() OVER(ORDER BY gn.GroupId ASC) AS INT)  Id,
		CAST(gn.GroupId AS INT) GroupId,
		gn.GroupName, 
		gn.GroupDesc,
		CAST(ISNULL((SELECT un.UserId FROM Usernames un WHERE un.Username = gn.GroupName), 0) AS INT) UserId,
		ISNULL((SELECT un.Username FROM Usernames un WHERE un.Username = gn.GroupName), '') Username,
		'' UserDesc
	FROM GroupNames gn 
	WHERE SUBSTRING(gn.GroupName, 1, 4) = 'CSIW'
	AND (@groupId = 0 OR gn.GroupId = @groupId)
END


IF @groupOnly = 2
BEGIN

	DECLARE @tempId INT = (SELECT CAST(gn.GroupId AS INT)
							FROM GroupNames gn 
							INNER JOIN UserGroupMap_mst ugm
							ON gn.GroupId = ugm.GroupId
							INNER JOIN Usernames un
							ON ugm.UserId = un.UserId
							WHERE un.UserId = @groupId)

	SELECT 
		CAST(ROW_NUMBER() OVER(ORDER BY gn.GroupId ASC) AS INT)  Id,
		CAST(gn.GroupId AS INT) GroupId,
		gn.GroupName, 
		gn.GroupDesc,
		CAST(un.UserId AS INT) UserId,
		un.Username,
		un.UserDesc
	FROM GroupNames gn 
	INNER JOIN UserGroupMap_mst ugm
	ON gn.GroupId = ugm.GroupId
	INNER JOIN Usernames un
	ON ugm.UserId = un.UserId
	WHERE SUBSTRING(gn.GroupName, 1, 4) = 'CSIW'
	AND (@groupId = 0 OR gn.GroupId = @tempId)
END







GO
