using csi_workflow.Server.Repository;
using csi_workflow.Shared;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace csi_workflow.Server.Controllers
{
    [Route("api/[controller]")]
    public class WorkflowController : Controller
    {
        ILogger _logger;
        IWorkflowRepository _workflowRepository;

        public WorkflowController(ILoggerFactory loggerFactory, IWorkflowRepository workflowRepository)
        {
            _logger = loggerFactory.CreateLogger(nameof(WorkflowController));
            _workflowRepository = workflowRepository;
        }


        [HttpGet("[action]/{id}/{status}")]
        public async Task<ActionResult> GetWorkflows([FromRoute] int id = 0, string status = "Open")
        {
            return Ok(await _workflowRepository.GetWorkflowsAsync(id, status));
        }

        
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult> GetWorkflow([FromRoute] int id)
        {
            return Ok(await _workflowRepository.GetWorkflowAsync(id));
        }


        [HttpPut("[action]")]
        public async Task<ActionResult> PutWorkflow([FromBody] Workflows workflow)
        {
            return Ok(await _workflowRepository.PutWorkflowAsync(workflow));
        }
    }
}
