using csi_workflow.Server.Repository;
using csi_workflow.Shared;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace csi_workflow.Server.Controllers
{
    [Route("api/[controller]")]
    public class GroupController : Controller
    {
        ILogger _logger;
        IGroupRepository _groupRepository;

        public GroupController(ILoggerFactory loggerFactory, IGroupRepository groupRepository)
        {
            _logger = loggerFactory.CreateLogger(nameof(GroupController));
            _groupRepository = groupRepository;
        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetGroups()
        {
            return Ok(await _groupRepository.GetGroupsAsync(1, 0));
        }


        [HttpGet("[action]/{id}")]
        public async Task<ActionResult> GetGroupsAndUsers([FromRoute] int id = 0)
        {
            return Ok(await _groupRepository.GetGroupsAsync(0, id));
        }


        [HttpGet("[action]/{id}")]
        public async Task<ActionResult> GetUsers([FromRoute] int id = 0)
        {
            return Ok(await _groupRepository.GetGroupsAsync(2, id));
        }
    }
}
