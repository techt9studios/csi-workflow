using System;
//using System.ComponentModel.DataAnnotations;

namespace csi_workflow.Shared
{
    public class Workflows
    {
        public int Id { get; set; }
        public string Task { get; set; }
        public string SalesOrder { get; set; }
        public int SalesOrderLine { get; set; }
        public string ItemNumber { get; set; }
        public string ItemDescription { get; set; }
        public DateTime DueDate { get; set; }
        public int Priority { get; set; }
        public string CreatedBy { get; set; }
        public string Owner { get; set; }
        //[Required]
        public decimal Hours { get; set; }
        public bool Completed { get; set; }
        public DateTime CompletedDate { get; set; }
        public string CSIId { get; set; } 
        public int UserId { get; set; }  
        public string Notes { get; set; }
        public int OwnerId { get; set; }
        
    }
}