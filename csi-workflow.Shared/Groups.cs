namespace csi_workflow.Shared
{
    public class Groups
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public string GroupDesc { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public string UserDesc { get; set; }
    }
}